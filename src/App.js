import "./App.css";
import { createBrowserHistory } from "history";

import { HomeTemplate } from "./templates/HomeTemplate/HomeTemplate";
import Home from "./pages/Home/Home";
import Contact from "./pages/Contact/Contact";
import News from "./pages/News/News";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import React from "react";
import Detail from "./pages/Detail/Detail";
import CheckoutTemplate from "./templates/CheckoutTemplate/CheckoutTemplate";
import { UserTemplate } from "./templates/UserTemplate/UserTemplate";
import Checkout from "./pages/Checkout/Checkout";
import { Suspense, lazy } from "react";
// const CheckoutTemplateLazy = lazy(() =>
//   import("./templates/CheckoutTemplate/CheckoutTemplate")
// );
export const history = createBrowserHistory();

function App() {
  return (
    <Router history={history}>
      <Switch>
        <HomeTemplate path="/home" exact Component={Home} />
        <HomeTemplate path="/news" exact Component={News} />
        <HomeTemplate path="/contact" exact Component={Contact} />
        <HomeTemplate path="/detail/:id" exact Component={Detail} />
        <HomeTemplate path="/login" exact Component={Login} />
        <Route path="/register" exact Component={Register} />

        <CheckoutTemplate path="/checkout/:id" exact component={Checkout} />
        {/* <UserTemplate path="/login" exact Component={Login} /> */}
        <HomeTemplate path="/" exact Component={Home} />
      </Switch>
    </Router>
  );
}

export default App;
{
  /* <Suspense fallback={<h1>LOADING...</h1>}>
          <CheckoutTemplateLazy
            path="/checkout/:id"
            exact
            component={Checkout}
          />
        </Suspense> */
}
