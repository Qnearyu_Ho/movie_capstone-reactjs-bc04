import { Redirect, Route } from "react-router";
import { USER_LOGIN } from "../../util/settings/config";
import { Fragment } from "react";
// import { Route } from "react-router-dom";
// import Footer from "./Layout/Footer/Footer";
// import Header from "./Layout/Header/Header";
// import HomeCarousel from "./Layout/HomeCarousel/HomeCarousel";
// import App from "../../App";
const CheckoutTemplate = (props) => {
  const { Component, ...restProps } = props;
  //   if (localStorage(USER_LOGIN)) {
  //     return <Redirect to="/login" />;
  //   }
  return (
    <Route
      {...restProps}
      render={(propsRoute) => {
        // props.location, props.history, props.match
        return (
          <Fragment>
            <Component {...propsRoute} />
          </Fragment>
        );
      }}
    />
  );
};

export default CheckoutTemplate;
