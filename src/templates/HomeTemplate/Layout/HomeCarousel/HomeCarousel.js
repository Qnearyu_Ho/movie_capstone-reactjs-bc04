import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useSelector, useDispatch } from "react-redux";
import axios from "axios";
import { getCarouselAction } from "../../../../redux/actions/CarouselActions";

export default function HomeCarousel(props) {
  const { arrImg } = useSelector((state) => state.CarouselReducer);
  const dispatch = useDispatch();
  console.log(arrImg);
  // Sẽ tự kích hoạt khi Component load ra
  // useEffect(async () => {
  //   try {
  //     const result = await axios({
  //       url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachBanner",
  //       method: "GET",
  //       headers: {
  //         TokenCybersoft:
  //           "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNCIsIkhldEhhblN0cmluZyI6IjA1LzAzLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Nzk3NDQwMDAwMCIsIm5iZiI6MTY1NDEwMjgwMCwiZXhwIjoxNjc4MTIyMDAwfQ.FunqYipkHrCbBATBzuJXyjGpZZxDekx1oY2qxW3_yfw",
  //       },
  //     });
  //
  //     console.log(result);
  //     dispatch({
  //       type: "SET_CAROUSEL",
  //       arrImg: result.data.content,
  //     });
  //   } catch (errors) {
  //     console.log(errors);
  //   }
  // }, []);
  // []: lần đầu tiên nó gọi thôi, chứ không phải mỗi lần render nó gọi
  useEffect(() => {
    // const action = getCarouselAction;
    dispatch(getCarouselAction(1));
    // 1: action = {type: '' , data}
    // 2 (phai cai middleware): callBackFunction (dispatch)
  }, []);
  const renderImg = () => {
    return arrImg.map((item, index) => {
      const contentStyle = {
        height: "650px",
        color: "#fff",
        lineHeight: "160px",
        textAlign: "center",
        backgroundPosition: "center",
        backgroundSize: "100%",
        backgroundRepeat: "no-repeat",
      };
      return (
        <div key={index}>
          <div
            style={{ ...contentStyle, backgroundImage: `url(${item.hinhAnh})` }}
          >
            <img src={item.hinhAnh} className="w-full opacity-0" />
          </div>
        </div>
      );
    });
  };
  return <Carousel effect="fade">{renderImg()}</Carousel>;
}
