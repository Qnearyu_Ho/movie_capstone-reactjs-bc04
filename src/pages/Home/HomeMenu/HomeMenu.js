import { Radio, Space, Tabs } from "antd";
import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import moment from "moment";
const { TabPane } = Tabs;
export default class Demo extends React.Component {
  state = {
    tabPosition: "left",
  };
  changeTabPosition = (e) => {
    this.setState({ tabPosition: e.target.value });
  };
  componentDidCatch() {
    console.log(this.props, "propsHeThongRapChieu");
  }
  renderHeThongRap = () => {
    return this.props.heThongRapChieu?.map((heThongRap, index) => {
      let { tabPosition } = this.state;
      return (
        <TabPane
          tab={
            <img src={heThongRap.logo} className="rounded-full" width="50" />
          }
          key={index}
        >
          <Tabs tabPosition={tabPosition}>
            {heThongRap.lstCumRap?.map((cumRap, index) => {
              return (
                <TabPane
                  tab={
                    <div style={{ width: "300px", display: "flex" }}>
                      <img
                        src={heThongRap.logo}
                        className="rounded-full"
                        width="50"
                      />
                      <div>
                        <div
                          className="ml-2"
                          style={{ justifyContent: "center" }}
                        >
                          {cumRap.tenCumRap}
                        </div>
                        <p
                          className="text-red-400 ml-2"
                          style={{ textAlign: "left" }}
                        >
                          Chi tiết
                        </p>
                      </div>
                    </div>
                  }
                  key={index}
                >
                  {cumRap.danhSachPhim.slice(0, 3).map((phim, index) => {
                    return (
                      <Fragment key={index}>
                        <div className="my-5" style={{ display: "flex" }}>
                          <div style={{ display: "flex" }}>
                            <img
                              className="mt-20"
                              width={100}
                              height={40}
                              src={phim.hinhAnh}
                              alt={phim.tenPhim}
                              onError={(e) => {
                                e.target.onerror = null;
                                e.target.src = "image_path_here";
                              }}
                            />
                            <div>
                              <h3 className="mt-20 ml-4 text-2xl font-bold text-black-500">
                                {phim.tenPhim}
                              </h3>
                              <p className="ml-4">{cumRap.diaChi}</p>
                              <div className="grid grid-cols-6 gap-5">
                                {phim.lstLichChieuTheoPhim
                                  ?.slice(0, 12)
                                  .map((lichChieu, index) => {
                                    return (
                                      <NavLink
                                        className="ml-4 text-orange-500 text-1xl"
                                        to="/"
                                        key={index}
                                      >
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("hh:mm A")}
                                      </NavLink>
                                    );
                                    // npm i moment
                                  })}
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr />
                      </Fragment>
                    );
                  })}
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  render() {
    console.log(this.props, "propsHeThongRapChieu");
    const { tabPosition } = this.state;
    return (
      <>
        <Tabs tabPosition={tabPosition}>{this.renderHeThongRap()}</Tabs>
      </>
    );
  }
}
