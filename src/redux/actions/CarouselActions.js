import { DOMAIN } from "../../util/settings/config";
import axios from "axios";
import { SET_CAROUSEL } from "./types/CarouselType";
import { quanLyPhimService } from "../../services/QuanLyPhimService";
export const getCarouselAction = () => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.layDanhSachBanner();
      console.log(result);
      dispatch({
        type: SET_CAROUSEL,
        arrImg: result.data.content,
      });
    } catch (errors) {
      console.log(errors);
    }
  };
};
